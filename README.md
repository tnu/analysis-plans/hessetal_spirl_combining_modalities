This repository contains the Analysis Plan for the project 'Speed-Incentivized Reward Learning Task'

Author: Alexander Hess
Contributors: Sandra Iglesias, Stefan Frässle, Laura Köchli, Stephanie Marino, Olivia Harrison, Lionel Rigoux, Klaas Enno Stephan

Version log

- v1: 14.04.2021 (background & analysis of the pilot data set)
- v2: 04.04.2022 (refinements of v1 & analysis of the main data set)

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
